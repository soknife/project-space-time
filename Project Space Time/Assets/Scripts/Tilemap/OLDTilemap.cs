﻿using UnityEngine;
using System.Collections;

public class OLDTilemap : MonoBehaviour {
	public Sprite[] tile_sprites;
	public int sprite_index;

	[Tooltip("Map details")]
	public Transform[,] map;
	public int map_x = 20;
	public int map_y = 20;

	[Tooltip("Size of the transform for each tile (8x8 pixels)")]
	public float tile_size = 1.0f;

	// Use this for initialization
	void Start() {
		mapInit();
	}

	//Functions to initialize map
	public void mapInit() {
		map = new Transform[map_x, map_y];
		build_map(map);
	}

	public void build_map(Transform[,] map) {
		for(int y = 0; y < map_y; y++) {
			for(int x = 0; x < map_x; x++) {
				Vector3 map_size = new Vector3(x * tile_size, y * tile_size, 0);
				Transform tile_map = new GameObject("Tile " + x + " " + y).transform;
				tile_map.transform.position = map_size;
				tile_map.gameObject.AddComponent<SpriteRenderer>().sprite = tile_sprites[sprite_index];
				tile_map.parent = transform;
				map[x, y] = tile_map;
			}
		}
	}

	
	// Update is called once per frame
	void Update() {
	
	}
}