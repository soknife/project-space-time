using UnityEngine;
using System.Collections;

[System.Serializable]
public class Character : MonoBehaviour {

    public CharacterControl controller;
    public CharacterState state;
    public Inventory inventory;
    public CharacterRender render;

	// Use this for initialization
	void Start () {
        Create();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    protected virtual void Create() {
        controller = GetComponent<CharacterControl>();
        state = GetComponent<CharacterState>();
        inventory = GetComponent<Inventory>();
        render = transform.FindChild("Rendering").GetComponent<CharacterRender>();
    }

    public virtual void Kill() {
        Destroy(this.gameObject);
    }
}
