using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
[System.Serializable]
public class CharacterRender : MonoBehaviour {

    public SpriteRenderer render;
    [Tooltip("Goes in order: up, down, right, left")]
    public Sprite[] sprites = new Sprite[4];

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // Switch to render front side
    public void Up() {
        render.sprite = sprites[0];
    }

    // Switch to render front side
    public void Down() {
        render.sprite = sprites[1];
    }

    // Switch to render front side
    public void Right() {
        render.sprite = sprites[2];
    }

    // Switch to render front side
    public void Left() {
        render.sprite = sprites[3];
    }

}
