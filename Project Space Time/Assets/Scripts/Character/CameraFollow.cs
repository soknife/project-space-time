using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public GameObject target;
    public float z_distance = -10f;

    void Start() {
        if (target == null)
            target = GameObject.Find("PLAYER");
        transform.position = new Vector3(0, 0, z_distance);
        transform.Translate((Vector2)target.transform.position - (Vector2)transform.position);
    }

    void LateUpdate() {
        transform.Translate((Vector2)target.transform.position - (Vector2)transform.position);
    }
}
