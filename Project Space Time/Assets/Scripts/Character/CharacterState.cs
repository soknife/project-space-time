using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterState : MonoBehaviour {

    Character character;

    public int max_health;
    public int health;

    public int attack;
    public int defense;

	// Use this for initialization
	void Start () {
        character = GetComponent<Character>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Damage(int amount) {
        health -= amount;
        if (health <= 0) {
            health = 0;
            character.Kill();
        }
        if (health > max_health)
            health = max_health;
    }

}
