using UnityEngine;
using System.Collections;

[System.Serializable]
public class AICharacter : Character {

    public GameObject overhead_health;

	// Use this for initialization
	void Start () {
        Create();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected override void Create() {
        base.Create();
        overhead_health = new GameObject();
        overhead_health.AddComponent<OverheadHealthBar>().Create(this);
    }

    public override void Kill() {
        base.Kill();
        Destroy(overhead_health);
    }
}
