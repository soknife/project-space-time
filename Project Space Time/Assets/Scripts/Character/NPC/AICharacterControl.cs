using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AICharacterControl : CharacterControl {

    PlayerCharacter PLAYER;
    bool attacking = false;
    Tile attack_tile = null;

	// Use this for initialization
	void Start () {
        PLAYER = GameObjectUtil.FindObjectComponent<PlayerCharacter>("PLAYER");
        Create();
	}
	
	// Update is called once per frame
	void Update () {
        Controller();
	}

    protected override void Create() {
        base.Create();
    }

    // Move character to position using smoothing translation
    public override void MovePosition(Tile target) {
        if (in_translation)
            return; // Still moving between other tiles
        if (!SetTile(target))
            return; // Invalid tile to move to
        SetTile(target);
        in_translation = true;
        current_destination = (Vector2)map.gameObject.transform.position + (Vector2)target.transform.position;
    }

    protected override void Controller() {
        if (current_tile == null)
            SetPosition((Vector2)transform.position);
        if (!in_translation) {
            // Set path finding
            if (PLAYER == null)
                PLAYER = GameObjectUtil.FindObjectComponent<PlayerCharacter>("PLAYER");
            if ((PLAYER.gameObject.transform.position - gameObject.transform.position).magnitude > 10)
                return;
            Tile[] path = map.path_finder.FindPath(current_tile, PLAYER.controller.Location);
            if (path != null && path.Length > 0)
                MovePosition(path[0]);

            // Attck
            if (!attacking) {
                int index = new List<Vector2>(current_tile.Adjacent).FindIndex(
                    x => x == PLAYER.controller.Location.Coordinate);
                if (index != -1) {
                    attacking = true;
                    attack_tile = map.GetTile(current_tile.Adjacent[index]);
                }
            }
            Attack(1);
        }
        
        SmoothTranslation();
    }

    protected override void OnTileUpdate() {
    }

    public override void Attack(int damage) {
        if (!attacking)
            return;
        // Pause before attack
        if (attack_dt < attack_cd) {
            attack_dt += Time.deltaTime;
            return;
        } else {
            attack_dt = 0f;
            attacking = false;
            if (attack_tile == null)
                return;
            int index = new List<Vector2>(current_tile.Adjacent).FindIndex(
                x => x == PLAYER.controller.Location.Coordinate);
            if (index == -1)
                return;
            new GameObject("attack_decal").AddComponent<AttackDecal>().Create(current_tile.Adjacent[index], attack_sprites[index]);
            map.GetTile(current_tile.Adjacent[index]).character.state.Damage(damage);
        }    
    }
}
