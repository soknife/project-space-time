using UnityEngine;
using System.Collections;

public class PlayerCharacter : Character {

    public OverlayHealthBar overlay_health;

	// Use this for initialization
	void Start () {
        Create();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.K))
            state.Damage(1);
	}

    protected override void Create() {
        base.Create();
        overlay_health = GameObjectUtil.FindObjectComponent<OverlayHealthBar>("Health Bar");
        overlay_health.Create(this);
    }

    public override void Kill() {
        Application.LoadLevel(Application.loadedLevel);
    }
}
