using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterControl : MonoBehaviour {

    protected enum Direction {
        up,
        down,
        right,
        left
    }

    [SerializeField]
    protected Tile current_tile; // Current position on map
    public Character character;

    public Map map; // Current map
    public float movement_speed; // Units per second

    [SerializeField]
    protected bool in_translation = false;
    [SerializeField]
    protected Vector2 current_destination;
    [SerializeField]
    protected Direction current_direction;

    public float attack_cd;
    protected float attack_dt = 0;
    public Sprite[] attack_sprites = new Sprite[4];

    public bool lighting_enabled = false;
    public int lighting_radius = 5;
    FloodLighting lighting;

	void Start () {
        Create();
        lighting = this.gameObject.AddComponent<FloodLighting>();
        lighting.ToggleLighting(lighting_enabled);
        if (lighting_enabled)
            lighting.GenerateLighting(map, map.GetTile(transform.position), lighting_radius);
	}
	
	void Update () {
        Controller();
	}

    protected virtual void Create() {
        if (map == null)
            map = GameObjectUtil.FindObjectComponent<Map>("MAP");
        SetPosition((Vector2)transform.position);
        current_direction = Direction.down;
    } 

    // Applys smooth translation between two tiles
    protected void SmoothTranslation() {
        if (!in_translation)
            return;
        // Target direction vector
        Vector2 target_vector = current_destination - (Vector2)transform.position;
        // If movement will over step
        if (movement_speed * Time.deltaTime > target_vector.magnitude) {
            transform.Translate(target_vector);
            in_translation = false;
            return;
        }
        // Move towards destination at speed
        target_vector = target_vector.normalized * movement_speed * Time.deltaTime;
        transform.Translate(target_vector);
    }

    // Move character to position using smoothing translation
    public virtual void MovePosition(Tile target) {
        if (in_translation)
            return; // Still moving between other tiles
        if (!SetTile(target))
            return; // Invalid tile to move to
        if (lighting_enabled)
            lighting.GenerateLighting(map, target, lighting_radius);
        SetTile(target);
        in_translation = true;
        current_destination = (Vector2)map.gameObject.transform.position + (Vector2)target.transform.position;
    }

    public void MovePosition(Vector2 coordinate) {
        MovePosition(map.GetTile(coordinate));
    }

    // Teleport character to position instantly
    public virtual void SetPosition(Tile target) {
        if (!SetTile(target))
            return; // Invalid tile to move to
        if (in_translation)
            in_translation = false; // Cancel current translation
        SetTile(target);
        transform.Translate((Vector2)target.transform.position - (Vector2)transform.position);
    }

    public void SetPosition(Vector2 coordinate) {
        SetPosition(map.GetTile(coordinate));
    }

    // Assigns new tile the character is located
    protected bool SetTile(Tile target) {
        if (target == null)
            return false;
        if (!target.AddCharacter(character))
            return false;
        if (current_tile != null)
            current_tile.RemoveCharacter();
        current_tile = target;
        OnTileUpdate();
        return true;
    }

    // Action performed when character is on a new tile
    protected virtual void OnTileUpdate() {
        if (current_tile.item != null && current_tile.item.lootable) {
            new GameObject().AddComponent<PopUpText>().Create(
                current_tile.item.name, this.transform.position + Vector3.up * .5f, Color.blue);
            character.inventory.AddItem(current_tile.item);
            current_tile.RemoveItem();
        }
    }

    // How this character is controlled
    protected virtual void Controller() {
        if (Input.GetKey(KeyCode.UpArrow)) {
            if (Input.GetKey(KeyCode.LeftShift)) {
                character.render.Up();
                current_direction = Direction.up;
            } else
                MoveUp();
        }

        if (Input.GetKey(KeyCode.DownArrow)) {
            if (Input.GetKey(KeyCode.LeftShift)) {
                character.render.Down();
                current_direction = Direction.down;
            } else
                MoveDown();
        }

        if (Input.GetKey(KeyCode.RightArrow)) {
            if (Input.GetKey(KeyCode.LeftShift)) {
                character.render.Right();
                current_direction = Direction.right;
            } else
                MoveRight();
        }

        if (Input.GetKey(KeyCode.LeftArrow)) {
            if (Input.GetKey(KeyCode.LeftShift)) {
                character.render.Left();
                current_direction = Direction.left;
            } else
                MoveLeft();
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            Attack(character.state.attack);
        }
        SmoothTranslation();

        if (attack_dt > 0) {
            attack_dt -= Time.deltaTime;
        }
        
    }

    public virtual void Attack(int damage) {
        if (attack_dt > 0) {
            return;
        }
        attack_dt = attack_cd;
        Vector2 attack_coord = -Vector2.one;;
        Sprite attack_sprite = attack_sprites[2];
        switch (current_direction) {
            case Direction.up:
                attack_coord = current_tile.Up;
                attack_sprite = attack_sprites[0];
                break;
            case Direction.down:
                attack_coord = current_tile.Down;
                attack_sprite = attack_sprites[1];
                break;
            case Direction.right:
                attack_coord = current_tile.Right;
                attack_sprite = attack_sprites[2];
                break;
            case Direction.left:
                attack_coord = current_tile.Left;
                attack_sprite = attack_sprites[3];
                break;
            default:
                break;
        }
        Tile attack_tile = map.GetTile(attack_coord);
        if (attack_tile == null)
            return;
        new GameObject("attack_decal").AddComponent<AttackDecal>().Create(attack_coord, attack_sprite);
        if (attack_tile.character == null)
            return;
        attack_tile.character.state.Damage(damage);
    }

    public virtual void MoveUp() {
        character.render.Up();
        current_direction = Direction.up;
        if (map.ValidMovementTile(current_tile.Up)) {
            MovePosition(current_tile.Up);
        }
    }

    public virtual void MoveDown() {
        character.render.Down();
        current_direction = Direction.down;
        if (map.ValidMovementTile(current_tile.Down)) {
            MovePosition(current_tile.Down);
        }
    }

    public virtual void MoveRight() {
        character.render.Right();
        current_direction = Direction.right;
        if (map.ValidMovementTile(current_tile.Right)) {
            MovePosition(current_tile.Right);
        }
    }

    public virtual void MoveLeft() {
        character.render.Left();
        current_direction = Direction.left;
        if (map.ValidMovementTile(current_tile.Left)) {
            MovePosition(current_tile.Left);
        }
    }

    public Tile Location { get { return current_tile; } }

}
