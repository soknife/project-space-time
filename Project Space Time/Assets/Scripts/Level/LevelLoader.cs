using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MapGenerator))]
public class LevelLoader : MonoBehaviour {

    public enum LevelLoading {
        New,
        Existing,
        Load
    }

    Level current_level;

    public LevelLoading load_levels;
    public List<Level> levels;
    public Vector2 default_size;
    public TilePrefab default_tile;
    public Sprite default_lighting;
    public PlayerCharacter default_player;

    public bool generate_random = true;
	public int number_of_rooms;
	public TilePrefab[] tile_array = new TilePrefab[3];
    
	void Awake () {

        if (load_levels == LevelLoading.New) {
            CreateLevel();
        } else if (load_levels == LevelLoading.Existing) {
            UseLevel();
        } else {
            LoadLevel(levels[0]);
        }
	}

    // Creates level from scratch
    public void CreateLevel() {
        RemoveLevel();
        current_level = new GameObject("LEVEL").AddComponent<Level>();
        CreateMap();
    }

    // Uses existing level
    public void UseLevel() {
        current_level = GameObjectUtil.FindObjectComponent<Level>("LEVEL");

    }

    // Loads level from prefab
    public void LoadLevel(Level level) {
        if (level == null)
            return;
        RemoveLevel();
        GameObject new_level = (GameObject)Instantiate(level.gameObject);
        new_level.name = "LEVEL";
        new_level.transform.position = new Vector3(0, 0, 25);
        current_level = new_level.GetComponent<Level>();
        Debug.Log("Level loaded");
    }

    // Removes level
    public void RemoveLevel() {
        DestroyImmediate(GameObject.Find("LEVEL"));
        if (current_level != null) {
            DestroyImmediate(current_level.gameObject);
            current_level = null;
        }
    }

    // Create a blank map using default size and tile
    public void CreateMap() {
        RemoveMap();
        GameObject new_map = new GameObject("MAP");
        
        new_map.transform.position = new Vector3(0, 0, 25);
        new_map.transform.SetParent(current_level.transform);

        if (generate_random) {
            MapGenerator map_generator = this.GetComponent<MapGenerator>();
            map_generator.Create(new_map.AddComponent<Map>());
			new_map.GetComponent<Map>().GenerateTileMap(map_generator, 
                default_size, tile_array, default_lighting, number_of_rooms);
            Vector2 spawn_point = Vector2.zero;
            do {
                spawn_point = (Vector2)map_generator.RandomRoomPoint(
                map_generator.RandomRoom());
            } while (new_map.GetComponent<Map>().GetTile(spawn_point).character != null);
            new_map.GetComponent<Map>().SpawnCharacter(default_player, spawn_point, "PLAYER");
        } else {
            new_map.AddComponent<Map>().GenerateTileMap(default_size, default_tile, default_lighting);
            new_map.GetComponent<Map>().SpawnCharacter(default_player, Vector3.zero, "PLAYER");
        }
        current_level.map = new_map.GetComponent<Map>();
    }

    // Loads map prefab
    public bool LoadMap(Map map) {
        if (map == null)
            return false;
        RemoveMap();
        GameObject new_map = (GameObject) Instantiate(map.gameObject);
        new_map.name = "MAP";
        new_map.transform.position = new Vector3(0, 0, 25);
        current_level.map = new_map.GetComponent<Map>();
        return true;
    }

    // Destory current map (or any lingering object named "MAP")
    void RemoveMap() {
        DestroyImmediate(GameObject.Find("MAP"));
        if (current_level.map != null) {
            DestroyImmediate(current_level.map.gameObject);
            current_level.map = null;
        }
    }
}
