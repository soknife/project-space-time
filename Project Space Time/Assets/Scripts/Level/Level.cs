using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Level : MonoBehaviour {
    public Map map;

    void Start() {
        
        // Get all characters in scene
        Character[] current_char = GameObject.FindObjectsOfType<Character>();
        // Set character parent to level if not already
        foreach (Character c in current_char)
            c.transform.SetParent(this.transform);
    }
}
