using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.Collections.Generic;

public class LevelEditor : MonoBehaviour {

#if UNITY_EDITOR
    public Level level; // Current level being edited
    public TilePrefab[] tile_prefabs; // Tile prefabs to load
    public Item[] item_prefabs; // Item prefabs to load

    List<Tile> tile_display = new List<Tile>();
    List<Tile> item_display = new List<Tile>();
    Tile selected_tile = null;
    Item selected_item = null;
    
	void Start() {

        if (level == null)
            level = GameObjectUtil.FindObjectComponent<Level>("LEVEL");

        // Draw tile prefabs
        for (int i = 0; i < tile_prefabs.Length; i++) {
            tile_display.Add(new GameObject("Tile prefab " + i).AddComponent<Tile>());
            tile_display[i].Create(
                gameObject, new Vector2(0, i), Vector2.zero, tile_prefabs[i]);
            tile_display[i].SetSorting("UI", 1);
        }

        // Draw item prefabs
        for (int i = 0; i < item_prefabs.Length; i++) {
            item_display.Add(new GameObject("Item prefab " + i).AddComponent<Tile>());
            item_display[i].Create(
                gameObject, new Vector2(-1.25f, i), Vector2.zero, 0, new Sprite(), null, item_prefabs[i]);
            item_display[i].SetSorting("UI", 1);
        }
	}

    void Update() {

        if (Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.P)) {
            SavePrefab();
        }

        if (Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.I)) {
            SaveBitmap();
        }

        if (Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.B)) {
            SaveBytes();
        }

        RightClickAction();
        LeftClickAction();
	}

    void RightClickAction() {
        // Right click
        if (Input.GetMouseButton(0)) {
            RaycastHit2D hit = Physics2D.GetRayIntersection(
                Camera.main.ScreenPointToRay(Input.mousePosition), Mathf.Infinity);

            if (hit && hit.collider.gameObject.GetComponent<Tile>() != null) {
                
                Tile hit_tile = hit.collider.gameObject.GetComponent<Tile>();
                // Selected prefab
                if (hit_tile.gameObject.transform.parent == gameObject.transform) {
                    // Prefab tile
                    if (hit_tile.item == null && selected_tile != hit_tile) {
                        selected_tile = hit_tile;
                        selected_item = null;
                    }
                        // Prefb item
                    else if (selected_item != hit_tile.item) {
                        selected_item = hit_tile.item;
                        selected_tile = null;
                    }
                }
                // Selected map tile with prefab tile
                else if (selected_tile != null && hit_tile != selected_tile) {
                    hit_tile.CopyTile(selected_tile);
                }
                // Selected map tile with prefab item
                else if (selected_item != null && hit_tile.item != selected_item) {
                    hit_tile.AddItem(selected_item);
                }
            }
        }
    }

    void LeftClickAction() {
        // Left click
        if (Input.GetMouseButton(1)) {
            Vector3 origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(
                new Ray(origin, Vector3.forward), Mathf.Infinity);

        if (hit && hit.collider.gameObject.GetComponent<Tile>() != null) {
            Tile hit_tile = hit.collider.gameObject.GetComponent<Tile>();
            // Selected map tile
            if (hit_tile.gameObject.transform.parent != gameObject.transform) {
                // Remove item if any
                if (hit_tile.item != null) {
                    hit_tile.RemoveItem();
                }
            }
        }
        }
    }

    // Saves the current map gameobject to prefab with name as current data/time
    void SavePrefab() {
        PrefabUtility.CreatePrefab("Assets/Prefabs/level_" + DateTime.Now.ToString("MM_dd_HH_mm_ss") + ".prefab", level.gameObject);
    }

    void SaveBitmap() {

    }

    void SaveBytes() {

    }

#endif
}
