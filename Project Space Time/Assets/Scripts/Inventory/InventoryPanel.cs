using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(RectTransform))]
public class InventoryPanel : MonoBehaviour {

    public Inventory inventory; // Player inventory
    RectTransform panel_transform; // Panel rect transform

    int capacity; // Inventory capacity
    Vector2 reference; // Target size for panel (size in editor)
    List<InventoryCell> cells; // List of item cells
    InventoryCell armor_cell;
    InventoryCell weapon_cell;

    Vector2 layout; // Inventory layout
    Vector2 screen_size; // Screen size last frame (for tracking window size update)
    bool hidden = true; // Is the inventory panel hidden
    float default_distance;

    void Awake() {
        panel_transform = GetComponent<RectTransform>();
        cells = new List<InventoryCell>();
        screen_size = new Vector2(Screen.width, Screen.height);
        default_distance = panel_transform.parent.gameObject.GetComponent<Canvas>().planeDistance;
    }

	void Start() {
        if (inventory == null)
            inventory = GameObjectUtil.FindObjectComponent<Inventory>("PLAYER");
        capacity = inventory.capacity;
        OnWindowUpdate(true);
        HidePanel(1);
	}
	
	void Update() {
        // Check window size update
        if (screen_size.x != Screen.width || screen_size.y != Screen.height) {
            screen_size = new Vector2(Screen.width, Screen.height);
            OnWindowUpdate();
        }
	}

    // When window size changes
    public void OnWindowUpdate(bool new_capacity = false) {
        // Find current reference size
        reference = panel_transform.anchorMax - panel_transform.anchorMin;
        reference = new Vector2(screen_size.x * reference.x, screen_size.y * reference.y);
        // Find best layout
        layout = ClosestAspectRatio(capacity, reference.x / reference.y);
        // Transform panel and cell to fit
        CellBestSize(layout, reference, panel_transform);
        // Create cells
        CreateItemCells(new_capacity);
        // Draw cells
        UpdateItemCells();
        HidePanel(hidden ? 1 : 2);
    }

    // Updates contents of inventory panel
    public void UpdateItemCells() {
        foreach (InventoryCell c in cells)
            c.UpdateItem();
    }

    // Toggles panel hide, 0: toggle mode, 1: force hide, 2: force unhide
    public void HidePanel(int mode = 0) {
        if (mode == 1) {
            panel_transform.parent.gameObject.GetComponent<Canvas>().planeDistance = -1;
            hidden = true;
        } else if (mode == 2) {
            panel_transform.parent.gameObject.GetComponent<Canvas>().planeDistance = default_distance;
            hidden = false;
            UpdateItemCells();
        } else {
            if (hidden) {
                HidePanel(2);
            } else {
                HidePanel(1);
            }
        }
    }

    // Creates item grid in inventory panel
    void CreateItemCells(bool new_capacity = false) {
        if (new_capacity) {
            // Destorys previous cells
            foreach (InventoryCell cell in cells)
                Destroy(cell.gameObject);
            cells.Clear();
            cells.Capacity = 0;
        }

        // Get inventory items
        Item[] items = inventory.GetItems();
        
        // Cell layout position
        int x = 0, y = 0;
        for (int i = 0; i < capacity; i++) {
            if (new_capacity) {
                // Instantiate cell
                GameObject cell_obj = new GameObject("Slot " + i);
                InventoryCell cell = cell_obj.AddComponent<InventoryCell>();
                cell.CreateCell(gameObject, i);

                // Add to list
                cells.Add(cell);
            }

            // Current cell rect transform
            RectTransform subject = cells[i].rect_transform;
            // Wrap to next row
            if (x >= layout.x) {
                x = 0;
                y++;
            }
            // Top right anchor (cell_width, 1) - > (1, cell_height)
            subject.anchorMax = new Vector2(1f / layout.x * (x + 1), 1f - 1f / layout.y * y);
            // Bottom left anchor (0, 1 - cell_height) - > (1 - cell_width, 0)
            subject.anchorMin = new Vector2(1f / layout.x * x, 1f - 1f / layout.y * (y + 1));

            RectTransformUtil.FitToAnchor(subject);
            cells[i].UpdateSize();

            // Next cell
            x++;
        }
        
    }

    // Get factor pairs for number
    // Example: number = 4, return = {(1, 4), (2, 2), (4, 1)}
    Vector2[] GetFactors(int number) {
        List<Vector2> result = new List<Vector2>();
        for (int i = 1; i < number; i++)
            if (number % i == 0)
                result.Add(new Vector2(i, number / i));

        return result.ToArray();
    }

    // Find the closest aspect ratio between factor pairs of number, and target aspect
    // Example: number = 24, target aspect = 1.78, return = (6, 4)
    Vector2 ClosestAspectRatio(int number, float target_aspect) {
        // Get factor pairs of inventory_size
        Vector2[] factor_pairs = GetFactors(number);
        Vector2 best_pair = new Vector2();
        float best_difference = float.MaxValue;

        foreach (Vector2 pair in factor_pairs) {
            // Compare inventory panel aspect ratio with factor pair aspect ratio
            float comparison = Mathf.Abs(target_aspect - ((float)pair.x / pair.y));
            // Closest difference deemed best pair
            if (comparison < best_difference) {
                best_difference = comparison;
                best_pair = pair;
            }
        }

        return best_pair;
    }

    // Reshapes the transform using target to best fit panel layout of pair
    // Example: pair = (6, 4), transform.size = (lesser * pair.x, lesser * pair.y)
    void CellBestSize(Vector2 pair, Vector2 target, RectTransform rect_transform) {
        float cell_width = target.x / pair.x;
        float cell_height = target.y / pair.y;
        // Lesser between cell_width and cell_height
        float lesser;
        if (cell_height < cell_width)
            lesser = cell_height;
        else
            lesser = cell_width;

        // Difference in size
        Vector2 delta = new Vector2(target.x - pair.x * lesser, target.y - pair.y * lesser);

        // Resize
        rect_transform.offsetMax = new Vector2(1 - delta.x / 2, 1 - delta.y / 2);
        rect_transform.offsetMin = new Vector2(delta.x / 2, delta.y / 2);
    }
}
