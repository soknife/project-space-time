using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryButton : MonoBehaviour {

    public InventoryPanel panel;

    void Start() {
        if (panel == null)
            panel = GameObjectUtil.FindObjectComponent<InventoryPanel>("Inventory Panel");
        panel.gameObject.SetActive(true);
    }

    void OnMouseUp() {
        panel.HidePanel();
    }
    
}
