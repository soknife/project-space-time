using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryCell : MonoBehaviour {

    public InventoryPanel panel; // Parent inventory panel
    public RectTransform rect_transform; // Cell rect transform
    public BoxCollider2D box_collider; // Collider (for OnMouse functions)
    public Canvas canvas; // Explicit canvas (for layer sorting)
    public Image image; // Item image display
    public Text text; // Item text display
    public int item_index = -1; // Index of item in inventory IMPORTANT: do NOT alter this after cell creation

    Vector2 mouse_position = Vector2.one * -1; // Mouse position tracking
    Vector2 original_position; // Original cell location

    // Blank inventory cell
    public void CreateCell(GameObject parent, int index) {
        // Parent
        this.panel = parent.GetComponent<InventoryPanel>();
        // Rect transform
        rect_transform = gameObject.AddComponent<RectTransform>();
        rect_transform.SetParent(parent.transform);
        rect_transform.localScale = Vector3.one;
        rect_transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        // Image
        image = new GameObject("Image", typeof(Image)).GetComponent<Image>();
        image.gameObject.transform.SetParent(gameObject.transform);
        image.transform.localScale = Vector3.one;
        image.transform.localPosition = Vector3.zero;
        image.rectTransform.anchorMax = new Vector2(0.9f, 0.9f);
        image.rectTransform.anchorMin = new Vector2(0.1f, 0.1f);
        RectTransformUtil.FitToAnchor(image.rectTransform);
        // Text 
        text = new GameObject("Text", typeof(Text)).GetComponent<Text>();
        text.gameObject.transform.SetParent(gameObject.transform);
        text.transform.localScale = Vector3.one;
        text.transform.localPosition = Vector3.zero;
        // Collider
        box_collider = gameObject.AddComponent<BoxCollider2D>();
        box_collider.size = new Vector2(image.rectTransform.rect.width, image.rectTransform.rect.height);
        // Canvas
        canvas = gameObject.AddComponent<Canvas>();
        canvas.overrideSorting = true;
        canvas.sortingLayerName = "UI";
        // Item
        item_index = index;
        UpdateItem();
    }

    // Update size for when parent panel transforms
    public void UpdateSize() {
        box_collider.size = new Vector2(image.rectTransform.rect.width, image.rectTransform.rect.height);
    }

    // Update item in cell
    public void UpdateItem() {
        Item item = panel.inventory.GetItem(item_index);
        if (item == null) {
            image.sprite = null;
            text.text = "";
            return;
        }
        image.sprite = item.image;
        text.text = item.name;
    }

    void RemoveItem() {
        panel.inventory.RemoveItem((int)item_index);
        panel.UpdateItemCells();
    }

    // Swaps item content with another cell and update both cells contents
    // IMPORTANT: this does not swap the index assigned to the cells, but rather the content those index holds
    public void SwapCell(InventoryCell other) {
        panel.inventory.SwapItem(this.item_index, other.item_index);
        this.UpdateItem();
        other.UpdateItem();
    }

    // Hold and drag item cell
    void OnMouseDrag() {
        // New drag action
        if (mouse_position == Vector2.one * -1) {
            mouse_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            original_position = rect_transform.localPosition;
        } 
        // Still dragging
        else {
            Vector2 delta_position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - mouse_position;
            mouse_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            rect_transform.Translate(delta_position);
        }
        // Move sorting forward while dragging
        canvas.sortingOrder = 1;
    }

    // Drag and drop item cell
    void OnMouseUp() {
        // Is the mouse over another cell
        RaycastHit2D[] hits = Physics2D.GetRayIntersectionAll(
            Camera.main.ScreenPointToRay(Input.mousePosition), Mathf.Infinity);
        // Has contact, check if it's a cell
        foreach (RaycastHit2D hit in hits) {
            if (hit.collider.gameObject == this.gameObject)
                continue;
            InventoryCell other = hit.collider.gameObject.GetComponent<InventoryCell>();
            // Swap contents with other cell
            if (other) {
                SwapCell(other);
                break;
            }
        }

        // Move Cell back to original position and sorting
        rect_transform.localPosition = original_position;
        canvas.sortingOrder = 0;
        // Reset mouse tracking
        mouse_position = Vector2.one * -1;
    }

    void OnMouseOver() {
        if (Input.GetMouseButtonUp(1))
            if (panel.inventory.GetItem(item_index) != null) {
                new GameObject().AddComponent<PopUpText>().Create(panel.inventory.GetItem(item_index).use_hint, this.transform.position, Color.blue);
                panel.inventory.character.state.Damage(-1);
                RemoveItem();
            }
    }

}
