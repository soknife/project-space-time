using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventory : ItemDatabase {

    public Character character;
    public InventoryPanel panel;

    [Tooltip("Inventory capacity")]
    public int capacity = 24;

	void Awake() {
        this.Initialize();
	}

    void Start() {
        Create();
    }
    virtual protected void Create() {
        if (character == null)
            character = GameObjectUtil.FindObjectComponent<Character>("PLAYER");
        if (panel == null)
            panel = GameObjectUtil.FindObjectComponent<InventoryPanel>("Inventory Panel");
    }

    // Loads items from quick_load into items list
    override protected void Initialize() {
        for (int i = 0; i < capacity; i++) {
            // Initialize list with null first
            items.Add(null);
            // Add item from quick load
            if (i < quick_load.Length)
                this.AddItem(quick_load[i]);
        }
    }

    // Add item into list by replacing the first instance of a null item
    override public bool AddItem(Item item) {
        // No more null slots
        if (items.FindIndex(x => x == null) == -1) {
            return false;
        }
        // Duplicate unique item
        if (item.unique && base.FindItem(item.id) != null) {
            return false;
        }
        // Stack stackable item
        if (item.stackable && base.FindItem(item.name) != null) {
            base.FindItem(item.name).count++;
        } 
        // New item
        else {
            items[items.FindIndex(x => x == null)] = item;
        }
        panel.UpdateItemCells();
        return true;
    }

    // Swaps the contents of one index with another
    public void SwapItem(int index_a, int index_b) {
        Item temp_item = items[index_a];
        items[index_a] = items[index_b];
        items[index_b] = temp_item;
    }

    int FindItem(Item item) {
        int index = items.FindIndex(x => x == item);
        return index;
    }

    // Replace item with null
    override public bool RemoveItem(string name) {
        // Find first instance of item with name
        int index = items.FindIndex(x => x.name == name);
        if (index == -1)
            return false;
        // Replace that instance with null
        items[index] = null;
        return true;
    }

    // Replace all instances of item with null
    override public bool RemoveItem(uint id) {
        // Find all instances of item with id
        int index = items.FindIndex(x => x.id == id);
        if (index == -1)
            return false;
        while (index != -1) {
            // Replace all instances will null
            items[index] = null;
            index = items.FindIndex(x => x.id == id);
        }
        return true;
    }

    public void RemoveItem(Item item) {
        int index = items.FindIndex(x => x == item);
        if (index == -1)
            return;
        items[index] = null;
    }


    public void RemoveItem(int index) {
        if (index >= items.Count)
            return;
        items[index] = null;
    }

    public Item GetItem(int i) {
        if (i >= items.Count)
            return null;
        return items[i];
    }

    // Resets' inventory on death
    public void OnDeath() {
        items.Clear();
    }

}
