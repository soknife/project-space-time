using UnityEngine;
using System.Collections;

[System.Serializable] 
public class Tile : MonoBehaviour {

    [SerializeField]
    Map map; // Parent map
    [SerializeField]
    BoxCollider2D box_collider; // 2D collider

    public Item item; // Item on this tile
    public Character character; // Character on this tile
    public SpriteRenderer floor_render; // Floor tile sprite
    public SpriteRenderer item_render; // Item sprite
    public SpriteRenderer lighting;

    [SerializeField]
    uint type; // Tile type
    [SerializeField]
    Vector2 coordinate; // This tiles' coordinates
    [SerializeField]
    Vector2[] adjacent; // Adjancent tiles

    void Awake() {
        floor_render = GetComponent<SpriteRenderer>();
    }

    // Tile constructor from scratch
    public void Create(GameObject parent, Vector2 tile_coord, Vector2 map_size, 
        uint tile_type, Sprite tile_sprite, Sprite lighting_sprite = null, Item tile_item = null, Character tile_character = null) {
        // Parent map
        map = parent.GetComponent<Map>();

        // Type
        type = tile_type;

        // Coordinates
        coordinate = tile_coord;
        adjacent = new Vector2[4] { Vector2.one * -1, Vector2.one * -1, Vector2.one * -1, Vector2.one * -1 };
        if (coordinate.y < map_size.y - 1)
            adjacent[0] = new Vector2(coordinate.x, coordinate.y + 1);
        if (coordinate.y > 0)
            adjacent[1] = new Vector2(coordinate.x, coordinate.y - 1);
        if (coordinate.x < map_size.x - 1)
            adjacent[2] = new Vector2(coordinate.x + 1, coordinate.y);
        if (coordinate.x > 0)
            adjacent[3] = new Vector2(coordinate.x - 1, coordinate.y);

        // Transform
        gameObject.transform.parent = parent.transform;
        gameObject.transform.localPosition = new Vector3(coordinate.x * gameObject.transform.localScale.x
            , coordinate.y * gameObject.transform.localScale.y, 0);

        // Sprite
        floor_render = gameObject.AddComponent<SpriteRenderer>();
        floor_render.sprite = tile_sprite;
        floor_render.sortingLayerName = "Map";

        // Collider
        box_collider = gameObject.AddComponent<BoxCollider2D>();
        box_collider.size = Vector2.one;

        // Item
        item_render = new GameObject("Item").AddComponent<SpriteRenderer>();
        item_render.gameObject.transform.parent = this.transform;
        item_render.gameObject.transform.localPosition = Vector3.zero;
        item_render.sortingLayerName = "Map";
        item_render.sortingOrder = 1;
        AddItem(tile_item);

        // Lighting
        lighting = new GameObject("Lighting").AddComponent<SpriteRenderer>();
        lighting.gameObject.transform.SetParent(this.transform);
        lighting.gameObject.transform.localPosition = Vector3.zero;
        lighting.sprite = lighting_sprite;
        lighting.sortingLayerName = "Lighting";
        lighting.sortingOrder = 0;
        SetLighting(0);

        // Character TODO
        if (tile_character != null) {
            character = tile_character;
        }
    }
    
    // Tile constructor with prefab
    public void Create(GameObject parent, Vector2 tile_coord, Vector2 map_size,
        TilePrefab prefab_tile, Sprite lighting = null, Item tile_item = null, Character tile_character = null) {
        this.Create(parent, tile_coord, map_size, prefab_tile.type, prefab_tile.sprite, lighting,
            tile_item, tile_character);
    }

    // Assign this tile with character, returns false if occupied
    public bool AddCharacter(Character c) {
        if (character != null)
            return false;
        character = c;
        return true;
    }

    public void RemoveCharacter() {
        character = null;
    }

    // Assign and display item on tile
    public void AddItem(Item new_item) {
        if (new_item == null)
            return;
        item = new_item;
        item_render.sprite = new_item.image;
    }

    public void RemoveItem() {
        item = null;
        item_render.sprite = null;
    }


    // Copies the contents of source onto this tile
    public void CopyTile(Tile source) {
        type = source.Type;
        floor_render.sprite = source.floor_render.sprite;
    }

    public void SetSorting(string layer_name, int sorting_order) {
        floor_render.sortingLayerName = layer_name;
        floor_render.sortingOrder = sorting_order;
    }

    public void SetLighting(float alpha) {
        lighting.color = new Color(0, 0, 0, alpha);
    }

    public uint Type { get { return type; } set { type = value; } }

    public Vector2 Coordinate { get { return coordinate; } set { coordinate = value; } }

    public Vector2 Up { get { return adjacent[0]; } set { adjacent[0] = value; } }

    public Vector2 Down { get { return adjacent[1]; } set { adjacent[1] = value; } }

    public Vector2 Right { get { return adjacent[2]; } set { adjacent[2] = value; } }

    public Vector2 Left { get { return adjacent[3]; } set { adjacent[3] = value; } }

    public Vector2[] Adjacent { get { return adjacent; } }

    public Map ParentMap { get { return map; } }
}
