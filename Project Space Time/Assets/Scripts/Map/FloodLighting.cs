using UnityEngine;
using System.Collections;

public class FloodLighting : MonoBehaviour {

    bool lighting_enabled = true;

    public void ToggleLighting(bool toggle) {
        lighting_enabled = toggle;
    }

    // Alters the map to create fog of war effect
    public void GenerateLighting(Map map, Tile location, int radius) {
        if (!lighting_enabled)
            return;
        // Reset lighting
        foreach (Tile t in map.TileMap) {
            t.SetLighting(1);
        }
        // Set center location to lit
        Tile current = location;
        current.SetLighting(0);

        // Distance from center
        int distance = 0;

        // UP
        // Generate lighting directly up for radius tiles
        for (; distance < radius; distance++) {
            // Get above tile
            if (map.GetTile(current.Up) != null)  {
                map.GetTile(current.Up).SetLighting(0);
                if (map.ValidMovementTile(current.Up))
                    current = map.GetTile(current.Up);
                else
                    break;
            } else {
                break;
            }
        }
        // Go back down previous column of lighting and flood lighting side ways
        for (; distance > 0; distance--) {
            // Flood amount increases by 1 every tile closer to center
            // Flood amount at center is radius, at tip (max distance of radius) is 0
            int flood_amount = radius - distance;
            // Flood side ways
            Tile right = current, left = current;
            for (int j = 0; j < flood_amount; j++) {
                // Rightside
                if (map.GetTile(right.Right) != null) {
                    map.GetTile(right.Right).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(right.Right)))
                        right = map.GetTile(right.Right);
                }
                // Leftside
                if (map.GetTile(left.Left) != null) {
                    map.GetTile(left.Left).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(left.Left)))
                        left = map.GetTile(left.Left);
                }
            }
            // Repeat until back to center
            // Get below tile
            current = map.GetTile(current.Down);
        }

        // RIGHT
        current = location;
        distance = 0;
        // Generate lighting directly right for radius tiles
        for (; distance < radius; distance++) {
            // Get above tile
            if (map.GetTile(current.Right) != null) {
                map.GetTile(current.Right).SetLighting(0);
                if (map.ValidMovementTile(current.Right))
                    current = map.GetTile(current.Right);
                else
                    break;
            } else {
                break;
            }
        }
        // Go back down previous column of lighting and flood lighting side ways
        for (; distance > 0; distance--) {
            // Flood amount increases by 1 every tile closer to center
            // Flood amount at center is radius, at tip (max distance of radius) is 0
            int flood_amount = radius - distance;
            // Flood side ways
            Tile up = current, down = current;
            for (int j = 0; j < flood_amount; j++) {
                // Rightside
                if (map.GetTile(up.Up) != null) {
                    map.GetTile(up.Up).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(up.Up)))
                        up = map.GetTile(up.Up);
                }
                // Leftside
                if (map.GetTile(down.Down) != null) {
                    map.GetTile(down.Down).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(down.Down)))
                        down = map.GetTile(down.Down);
                }
            }
            // Repeat until back to center
            // Get below tile
            current = map.GetTile(current.Left);
        }

        // DOWN
        current = location;
        distance = 0;
        // Generate lighting directly down for radius tiles
        for (; distance < radius; distance++) {
            // Get above tile
            if (map.GetTile(current.Down) != null) {
                map.GetTile(current.Down).SetLighting(0);
                if (map.ValidMovementTile(current.Down))
                    current = map.GetTile(current.Down);
                else
                    break;
            } else {
                break;
            }
        }
        // Go back left previous column of lighting and flood lighting side ways
        for (; distance > 0; distance--) {
            // Flood amount increases by 1 every tile closer to center
            // Flood amount at center is radius, at tip (max distance of radius) is 0
            int flood_amount = radius - distance;
            // Flood side ways
            Tile right = current, left = current;
            for (int j = 0; j < flood_amount; j++) {
                // Rightside
                if (map.GetTile(right.Right) != null) {
                    map.GetTile(right.Right).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(right.Right)))
                        right = map.GetTile(right.Right);
                }
                // Leftside
                if (map.GetTile(left.Left) != null) {
                    map.GetTile(left.Left).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(left.Left)))
                        left = map.GetTile(left.Left);
                }
            }
            // Repeat until back to center
            // Get below tile
            current = map.GetTile(current.Up);
        }

        // LEFT
        current = location;
        distance = 0;
        // Generate lighting directly left for radius tiles
        for (; distance < radius; distance++) {
            // Get above tile
            if (map.GetTile(current.Left) != null) {
                map.GetTile(current.Left).SetLighting(0);
                if (map.ValidMovementTile(current.Left))
                    current = map.GetTile(current.Left);
                else
                    break;
            } else {
                break;
            }
        }
        // Go back down previous column of lighting and flood lighting side ways
        for (; distance > 0; distance--) {
            // Flood amount increases by 1 every tile closer to center
            // Flood amount at center is radius, at tip (max distance of radius) is 0
            int flood_amount = radius - distance;
            // Flood side ways
            Tile up = current, down = current;
            for (int j = 0; j < flood_amount; j++) {
                // Rightside
                if (map.GetTile(up.Up) != null) {
                    map.GetTile(up.Up).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(up.Up)))
                        up = map.GetTile(up.Up);
                }
                // Leftside
                if (map.GetTile(down.Down) != null) {
                    map.GetTile(down.Down).SetLighting(0);
                    if (map.ValidMovementTile(map.GetTile(down.Down)))
                        down = map.GetTile(down.Down);
                }
            }
            // Repeat until back to center
            // Get below tile
            current = map.GetTile(current.Right);
        }
    }
}
