using UnityEngine;
using System.Collections;

[System.Serializable] 
public class Map : MonoBehaviour {
    
    public Vector2 map_size;
    public Vector2 starting_position = Vector2.zero;
    public PathFinder path_finder;

    [SerializeField]
    string map_name;
    [SerializeField]
    public Tile[] tile_map;

    public void Start() {
        path_finder = new PathFinder(this, tile_map, (int)map_size.x, (int)map_size.y);
    }
    
    // Generates a blank tile map with size and populated with default tile
    public void GenerateTileMap(Vector2 size, TilePrefab default_tile, Sprite default_lighting) {
        map_size = size;
        tile_map = new Tile[(int)size.x * (int)size.y];
        int y = 0, x = 0;
        for (; y < size.y; y++) {
            for (x = 0; x < size.x; x++) {
                tile_map[y * (int)size.y + x] = new GameObject("Tile_" + x + "_" + y).AddComponent<Tile>();
                tile_map[y * (int)size.y + x].Create(gameObject, new Vector2(x, y), size, default_tile, default_lighting);
            }
        }
    }

	//Generates procedural tile map
	public void GenerateTileMap(MapGenerator map, Vector2 size, TilePrefab[] tile_array, Sprite default_lighting, int number_of_rooms) {
		map_size = size;
		tile_map = new Tile[(int)size.x * (int)size.y];
		map.BuildLevel((int)size.x, (int)size.y, number_of_rooms);

		for(int y = 0; y < size.y; y++) {
			for(int x = 0; x < size.x; x++) {

				tile_map[y * (int)size.y + x] = new GameObject("Tile_" + x + "_" + y).AddComponent<Tile>();
                if (map.map[x, y] == MapGenerator.TileType.empty) {
					tile_map[y * (int)size.y + x].Create(gameObject, new Vector2(x, y), size, tile_array[1], default_lighting);
                } else if (map.map[x, y] == MapGenerator.TileType.wall) {
					tile_map[y * (int)size.y + x].Create(gameObject, new Vector2(x, y), size, tile_array[2], default_lighting);
				}
				else {
					tile_map[y * (int)size.y + x].Create(gameObject, new Vector2(x, y), size, tile_array[0], default_lighting);
				}
				//Debug.Log (map.map[x, y]);
			}
		}

        map.PopulateMap();
	}

    public void SpawnCharacter (Character character, Vector2 position, string name) {
        GameObject new_character = (GameObject)Instantiate(character.gameObject, position, Quaternion.identity);
        new_character.name = name;
    }

    // Returns the reference to tile via coordinate
    public Tile GetTile(Vector2 coordinate) {
        if (tile_map == null)
            Debug.LogError("Get tile error, tile at " + coordinate + " does not exist");
        if (coordinate.x < 0 || coordinate.y < 0 ||
            coordinate.x >= map_size.x || coordinate.y >= map_size.y) {
            return null;
        }
        return tile_map[(int)coordinate.y * (int)map_size.x + (int)coordinate.x ];
    }

    // TEMP
    // Test if tile is valid for character
    public bool ValidMovementTile(Vector2 coordinate) {
        return ValidMovementTile(GetTile(coordinate));
    }

    // TEMP
    // Test if tile is valid for character
    public bool ValidMovementTile(Tile tile) {
        return tile != null ? tile.Type == 1 : false;
    }

    public Tile[] TileMap { get { return tile_map; } set { tile_map = value; } }

    public string MapName { get { return map_name; } set { map_name = value; } }
}
