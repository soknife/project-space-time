using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cell {
    public int x, y; // Coordinate
    public int H, G, F; // Heuristic, adjacent movement, and total movement cost
    public int parent; // Parent index
    public int closed; // Closed, open, or unchecked (1, 0, -1)
    public List<int> adj; // Adjacent indicies
    public bool impassable; // Impassable cell

    public Cell(int _x, int _y, int _H, int _G, int _F, int _parent, int _closed, int[] _adj, bool _impass) {
        x = _x;
        y = _y;
        H = _H;
        G = _G;
        F = _F;
        parent = _parent;
        closed = _closed;
        adj = new List<int>(_adj);
        impassable = _impass;
    }
}
