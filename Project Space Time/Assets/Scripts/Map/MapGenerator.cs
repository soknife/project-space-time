using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour{

    //map variables
    int map_x, map_y;
    public TileType[,] map;
    public List<Rectangle> room = new List<Rectangle>();

    Map MAP; // global map reference
    public int item_amount = 0;
    public Item[] available_items;
    public int mob_amount = 0;
    public AICharacter[] available_mobs;

	//Numeric representation of tiles
	public enum TileType {
		floor,
        empty,
        wall,
        none
	}

    // Rectangle 
    // Why you make this? Rect and RectTransform already exists lol
	public struct Rectangle {
		public int xMin, xMax, yMin, yMax;
		public Point coordinates;

		public Rectangle(int x, int xx, int y, int yy, Point c) {
			xMin = x;
			xMax = xx;
			yMin = y;
			yMax = yy;
			coordinates = c;
		}

        public int Width { get { return xMax - xMin; } }

        public int Height { get { return yMax - yMin; } }

        public int InnerWidth { get { return Width - 2; } }

        public int InnerHeight { get { return Height - 2; } }

        public Point Max { get { return new Point(xMax, yMax); } }

        public Point Min { get { return new Point(xMin, yMin); } }

        public Point InnerMax { get { return new Point(xMax - 1, yMax - 1); } }

        public Point InnerMin { get { return new Point(xMin + 1, yMin + 1); } }
	}

	//integer vector2
	public struct Point {
		public int x, y;
		
		public Point(int px, int py) {
			x = px;
			y = py;
		}

        public static explicit operator Vector2(Point self) {
            return new Vector2(self.x, self.y);
        }
	}

    public void Create(Map map) {
        MAP = map;
    }

	#region Accessible functions
	//Builds fully generated map
	public void BuildLevel(int x, int y, int number_of_rooms) {
		map = new TileType[x, y];

        map_x = x;
        map_y = y;

		//Creates a blank map
		NewMap(TileType.wall, x, y);

		//spawn point
		//Point origin = new Point(0, 0);
		//Rectangle spawn = new Rectangle(0, 1, 0, 1, origin);
		//room.Add(spawn);

		//Creates rooms
		Rooms(x, y, number_of_rooms);
		Tunnels(x, y);
	}

	//Fills floor
	public void NewMap(TileType type, int x, int y) {
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				map[i, j] = type;
			}
		}
	}

	#endregion

	#region Room functions
	//Generates room
	public void Rooms(int x, int y, int number_of_rooms) {
		int placed = 0;
		int debug_count = 0;

		int min = (int)(Mathf.Min(x, y) / 4f);
		int max = (int)(Mathf.Sqrt(x * y) / 10f);

		while(placed < number_of_rooms) {
			//Picks random coordinates padded around the map edge
			int cx = Random.Range(1, x);
			int cy = Random.Range(1, y);
			Point new_coordinates = new Point(cx, cy);

			//creates new room (xMin, xMax, yMin, yMax, coordinates);
			Rectangle new_room = new Rectangle(0, Random.Range(min, max), 0, Random.Range(min, max), new_coordinates);
			//Debug.Log (new_room.xMax + " " +new_room.yMax + " " + new_room.coordinates.x + " " + new_room.coordinates.y);

			if(PlaceRoom(x, y, new_room)) {
				placed++;
			}
			//debug
			debug_count++;
			if(debug_count > 1000) {
				break;
			}
		}
	}

	//Checks for placeable and adds to list
	public bool PlaceRoom(int width, int height, Rectangle current_room) {
		bool placeable = FreeArea(current_room, width, height);
		//Debug.Log (placeable);
		//Debug.Log ((current_room.coordinates.x+current_room.xMax) + " " + (current_room.coordinates.y+current_room.yMax));
		if(placeable == true) {
			room.Add(current_room);
			NewRoom(current_room);
			return true;
		}
		else {
			return false;
		}
	}

	//Fills new room in on the map
	public void NewRoom(Rectangle current_room) {
		//Fill with floor tiles
		for(int x = current_room.xMin; x < current_room.xMax; x++) {
			for(int y = current_room.yMin; y < current_room.yMax; y++) {
				map[(current_room.coordinates.x + x), (current_room.coordinates.y + y)] = TileType.floor;
			}
		}

		//Wrap with wall tiles
		for(int x = current_room.xMin; x <= current_room.xMax; x++) {
			map[(current_room.coordinates.x + x), current_room.coordinates.y + current_room.yMin] = TileType.wall;
			map[(current_room.coordinates.x + x), current_room.coordinates.y + current_room.yMax] = TileType.wall;
		}

		for(int y = current_room.yMin; y <= current_room.yMax; y++) {
			map[current_room.coordinates.x + current_room.xMin, current_room.coordinates.y + y] = TileType.wall;
			map[current_room.coordinates.x + current_room.xMax, current_room.coordinates.y + y] = TileType.wall;
		}

		/*
		//Create entrances
		int doors = 2;
		for(int i = 0; i < doors; i++) {
			int face = Random.Range(0,4);
			if(face == 0) {
				map[current_room.coordinates.x + current_room.xMin, 
				    Random.Range(current_room.coordinates.y + current_room.yMin+1, 
				    	current_room.coordinates.y + current_room.yMax)] = TileType.floor;
			}
			else if(face == 1) {
				map[current_room.coordinates.x + current_room.xMax, 
				    Random.Range(current_room.coordinates.y + current_room.yMin+1, 
				    	current_room.coordinates.y + current_room.yMax)] = TileType.floor;
			}
			else if(face == 2) {
				map[Random.Range(current_room.coordinates.x + current_room.xMin+1, 
					current_room.coordinates.x + current_room.xMax), 
				    	current_room.coordinates.y + current_room.yMin] = TileType.floor;
			}
			else {
				map[Random.Range(current_room.coordinates.x + current_room.xMin+1, 
					current_room.coordinates.x + current_room.xMax), 
				    	current_room.coordinates.y + current_room.yMax] = TileType.floor;
			}
		}
		*/
	}

	#endregion

	#region Tunnels
	//Tunnelling
	public void Tunnels(int x, int y) {
		List<Point> room_coordinates = new List<Point>();
		for(int i = 0; i < room.Count; i++) {
			room_coordinates.Add(new Point(room[i].coordinates.x + (int)(room[i].xMax/2), 
			                               room[i].coordinates.y + (int)(room[i].yMax/2)));

			//Debug.Log ("room coord: " + room_coordinates[i].x + " " + room_coordinates[i].y);
		}


		Pathfind(room_coordinates);
	}

	//Pathfinding from one room to another 
	public void Pathfind(List<Point> coordinates) {
		for(int i = 1; i < coordinates.Count; i++) {
			int x = coordinates[i].x - coordinates[i-1].x;
			int y = coordinates[i].y - coordinates[i-1].y;

			//right or left
			if(x > 0) {
				for(int step = 0; step < x; step++) {
					//Debug.Log ("right: " + (coordinates[i-1].x + step));
					map[coordinates[i-1].x + step, coordinates[i-1].y] = TileType.floor;
					//Debug.Log (map[coordinates[i-1].x + step, coordinates[i-1].y]);
				}
			}
			else {
				for(int step = 0; step > x; step--) {
					//Debug.Log ("left: " + (coordinates[i-1].x + step));
					map[coordinates[i-1].x + step, coordinates[i-1].y] = TileType.floor;
					//Debug.Log (map[coordinates[i-1].x + step, coordinates[i-1].y]);
				}
			}

			//up or down
			if(y > 0) {
				for(int step = 0; step < y; step++) {
					//Debug.Log ("up: " + (coordinates[i-1].y + step));
					map[coordinates[i].x, coordinates[i-1].y + step] = TileType.floor;
					//Debug.Log (map[coordinates[i-1].x, coordinates[i-1].y + step]);
				}
			}
			else {
				for(int step = 0; step > y; step--) {
					//Debug.Log ("down: " + (coordinates[i-1].y + step));
					map[coordinates[i].x, coordinates[i-1].y + step] = TileType.floor;
					//Debug.Log (map[coordinates[i-1].x, coordinates[i-1].y + step]);
				}
			}
		}
	}

	#endregion

	#region Utilities
	//Checks for floor area
	public bool FreeArea(Rectangle current_room, int x, int y) {
		int width = current_room.coordinates.x + current_room.xMax + 2;
		int height = current_room.coordinates.y + current_room.yMax + 2;

		//x and y -2 for padding around map edge
		if(width > x-2 || height > y-2) {
			return false;
		}

		for(int i = current_room.coordinates.x - 1; i < width; i++) {
			for(int j = current_room.coordinates.y - 1; j < height; j++) {
				TileType current_tile = map[i, j];
				if(current_tile != TileType.empty && current_tile != TileType.wall) {
					return false;
				}
			}
		}
		return true;
	}

	#endregion

    // Populates map with mobs and items
    public void PopulateMap() {
        SpawnItems(item_amount);
        SpawnMobs(mob_amount);
    }

    // Return a random room
    public Rectangle RandomRoom() {
        int RNGESUS = Random.Range(1, room.Count) - 1;
        return room[RNGESUS];
    }

    // Return a random point within a room
    public Point RandomRoomPoint(Rectangle rect) {
        // Random point within room inner boundaries
        int rnd_x = rect.coordinates.x + Random.Range(rect.InnerMin.x + 1, rect.InnerMax.x + 1) - 1;
        int rnd_y = rect.coordinates.y + Random.Range(rect.InnerMin.y + 1, rect.InnerMax.y + 1) - 1;
        return new Point(rnd_x, rnd_y);
    }

    // Spawns items
    void SpawnItems(int amount) {
        // For every room
        foreach (Rectangle rect in room) {
            Point random_point = RandomRoomPoint(rect);
            // Random item from selection
            int RNGESUS_item = (int)Random.Range(1, available_items.Length) - 1;
            MAP.tile_map[Double2Single(random_point)].AddItem(available_items[RNGESUS_item]);
        }
    }

    // Spawns mobs
    void SpawnMobs(int amount) {
        for (int i = 0; i < amount; i++) {
            Point random_point = RandomRoomPoint(RandomRoom());
            int RNGESUS_mob = (int)Random.Range(1, available_mobs.Length) - 1;
            MAP.SpawnCharacter(available_mobs[RNGESUS_mob], (Vector2)random_point, available_mobs[RNGESUS_mob].name);
        }
    }

    // Converts 2d index to 1d index
    int Double2Single(Point double_index) {
        return double_index.y * map_y + double_index.x;
    }

    // Convert 1d index to 2d index
    Point Single2Double(int single_index) {
        return new Point(single_index / map_y, single_index % map_y);
    }

}
