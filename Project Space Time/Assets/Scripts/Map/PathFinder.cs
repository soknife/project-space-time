using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFinder {
    Map map;
    // Original tile map and cell map
    // Note both maps are in sync in terms of coordinates
    Cell[] cell_map;
    Tile[] tile_map;
    int width, height;

    public PathFinder(Map map, Tile[] tile_map, int map_width, int map_height) {
        this.map = map;
        width = map_width;
        height = map_height;
        this.tile_map = tile_map;
        
        
    }

    public Tile[] FindPath(Tile origin, Tile destination) {
        List<Cell> cells = new List<Cell>();
        // Convert map into Cell list
        for (int i = 0; i < width * height; i++) {
            List<int> _adj = new List<int>();
            // Get adjancency
            foreach (Vector2 v in tile_map[i].Adjacent) {
                // Always add destination
                if (destination.Coordinate == v) {
                    _adj.Add((int)v.y * height + (int)v.x);
                    continue;
                }
                // Ignore non existing or blocking tiles
                if (v == Vector2.one * -1 || tile_map[(int)v.y * height + (int)v.x].Type == 0)
                    continue;
                // Ignore characters with tiles
                else if (tile_map[(int)v.y * height + (int)v.x].character != null)
                    continue;
                // Otherwise add adjacent
                else
                    _adj.Add((int)v.y * height + (int)v.x);
            }
            // Create cell
            cells.Add(new Cell((int)tile_map[i].Coordinate.x,
                (int)tile_map[i].Coordinate.y,
                0, 0, 0, 0, 0, _adj.ToArray(), tile_map[i].Type == 0));
        }

        // List of open tiles considerable for path finding
        List<int> open_cells = new List<int>();
        // Final path
        List<Tile> path = new List<Tile>();

        // Calculate heutristics
        for (int i = 0; i < cells.Count; i++) {
            // Calculate Manhattan
            cells[i].H = Mathf.Abs(cells[i].x - (int)destination.Coordinate.x)
                + Mathf.Abs(cells[i].y - (int)destination.Coordinate.y);
        }

        // Set starting tile values
        int current_cell = 0;
        try {
            current_cell = cells.FindIndex(x => x.x == origin.Coordinate.x
                && x.y == origin.Coordinate.y);
        } catch (System.NullReferenceException e) {
            return null;
        }
        cells[current_cell].closed = 1;
        cells[current_cell].parent = current_cell;

        // Initial open list
        foreach (int i in cells[current_cell].adj) {
            // Movement cost (constant right now)
            cells[i].G += 1;
            // Total cost from start
            cells[i].F = cells[i].H + cells[i].G;
            // Parent node
            cells[i].parent = current_cell;
            // Mark as open
            cells[i].closed = -1;
            open_cells.Add(i);
        }

        // While there are available paths
        while (open_cells.Count > 0) {
            // Get lowest priority cell (lowest H + G value)
            int min_cost = cells[open_cells[0]].H + cells[open_cells[0]].G; // Minimum cost
            int min_index = open_cells[0]; // Minimum cost cell index
            int open_index = 0; // Minimum cost open list index
            for (int i = 0; i < open_cells.Count; i++) {
                if (cells[open_cells[i]].H + cells[open_cells[i]].G < min_cost) {
                    min_cost = cells[open_cells[i]].H + cells[open_cells[i]].G;
                    min_index = open_cells[i];
                    open_index = i;
                }
            }
            // Set current to lowest priority
            current_cell = min_index;

            // Destination is right next to cell
            if (new Vector2(cells[current_cell].x, cells[current_cell].y) == destination.Coordinate) {
                // Backtrack through parents for path
                path.Add(tile_map[current_cell]);
                current_cell = cells[current_cell].parent;

                // Reserver path order (start -> finish)
                path.Reverse();
                return path.ToArray();
            }
            
            // Remove lowest priority from open tiles list
            open_cells.RemoveAt(open_index);

            // Add adjacencies from lowest priority to open list
            foreach (int adj_index in cells[current_cell].adj) {
                // Check if destination
                if (new Vector2(cells[adj_index].x, cells[adj_index].y) == destination.Coordinate) {
                    // Set current cell to destination
                    cells[adj_index].parent = current_cell;
                    current_cell = adj_index;

                    // Backtrack through parents for path
                    while (current_cell != cells[current_cell].parent) {
                        path.Add(tile_map[current_cell]);
                        current_cell = cells[current_cell].parent;
                    }

                    // Reserver path order (start -> finish)
                    path.Reverse();
                    return path.ToArray();
                }
                
                // Unchecked cells
                if (cells[adj_index].closed == 0) {
                    // Movement cost (constant in this case)
                    cells[adj_index].G += 1;
                    // Total cost from start
                    cells[adj_index].F = cells[adj_index].H + cells[adj_index].G;
                    // Parent node
                    cells[adj_index].parent = current_cell;
                    // Mark as open
                    cells[adj_index].closed = -1;
                    // Add to open list
                    open_cells.Add(adj_index);
                }

                // Opened cells
                else if (cells[adj_index].closed == -1) {
                    // If moving from current cell to this cell costs less than previous
                    if (cells[current_cell].G + 1 < cells[adj_index].G)
                        cells[adj_index].parent = current_cell;
                }

                // Ignore closed cells
            }

            // Repeat adding adjacent cells with newly added adjancent cells

        }

        // No solution possible
        return new Tile[] { };
    }
}
