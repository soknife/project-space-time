using UnityEngine;
using System.Collections;

public static class MeshUtil {

    public static void CreateSquare(Mesh subject, Vector2 min, Vector2 max) {
        Vector3[] vert = new Vector3[4];
        int[] tri = new int[6];
        vert[0] = min;
        vert[2] = max;
        vert[1] = new Vector3(max.x, min.y);
        vert[3] = new Vector3(min.x, max.y);
        tri[0] = 0; tri[1] = 2; tri[2] = 3;
        tri[3] = 0; tri[4] = 1; tri[5] = 2;
        subject.vertices = vert;
        subject.triangles = tri;
    }

}
