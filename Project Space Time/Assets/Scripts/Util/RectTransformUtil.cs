using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class RectTransformUtil {

    
    public static void FitToParent(RectTransform child) {
        child.anchorMax = Vector2.one;
        child.anchorMin = Vector2.zero;
        child.offsetMax = Vector2.zero;
        child.offsetMin = Vector2.zero;
    }

    public static void FitToAnchor(RectTransform self) {
        self.offsetMax = Vector2.zero;
        self.offsetMin = Vector2.zero;
    }
}
