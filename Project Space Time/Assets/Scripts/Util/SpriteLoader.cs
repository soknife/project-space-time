using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteLoader : MonoBehaviour {

    [Header("Tile sprites")]
    public List<Sprite> tile_sprites;
    [Header("Character sprites")]
    public List<Sprite> character_spries;
    [Header("Item sprites")]
    public List<Sprite> item_sprites;


    
}
