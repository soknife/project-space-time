using UnityEngine;
using System;
using System.Collections;

public static class GameObjectUtil{

    // Safe method of finding a gameobject and its' component in the scene
    // Type T is automaticly checked and does not need to be included in param components
	public static T FindObjectComponent<T>(string object_name, params System.Type[] components) {
        // Check if gameobject exists
        GameObject subject = GameObject.Find(object_name);
        if (subject == null) {
            Debug.LogError("Essential object " + object_name + " not found in scene");
            return default(T);
        }

        // Check if requested return type is a component of gameobject
        if (subject.GetComponent(typeof(T)) == null) {
            Debug.LogError("Essential object " + object_name + " does not have component " + typeof(T).Name);
            return default(T);
        }

        // Check if remianing compoenets are included in gameobject
        foreach (System.Type t in components) {
            if (subject.GetComponent(t) == null) {
                Debug.LogError("Essential object " + object_name + " does not have component " + t.Name);
                return default(T);
            }
        }

        return (T)Convert.ChangeType(subject.GetComponent(typeof(T)), typeof(T));
    }
}
