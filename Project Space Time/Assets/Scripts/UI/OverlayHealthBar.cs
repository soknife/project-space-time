using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OverlayHealthBar : MonoBehaviour {

    Character target_character;
    RectTransform rect_transform;
    Image health_bar;

    public void Create(Character target) {
        target_character = target;
        rect_transform = GetComponent<RectTransform>();
        health_bar = GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        float percentage = (float)target_character.state.health / target_character.state.max_health * 1;
        rect_transform.localScale = new Vector3(percentage, 1, 1);
        if (percentage > 0.65f)
            health_bar.color = new Color32(119, 227, 85, 255);
        else if (percentage > 0.35f)
            health_bar.color = new Color32(227, 211, 85, 255);
        else
            health_bar.color = new Color((float)277 / 255, (float)85/255, (float)85/255, 1);
	}
}
