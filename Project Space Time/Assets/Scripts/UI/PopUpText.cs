using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopUpText : MonoBehaviour {

    RectTransform rect_transform;

    public void Create(string hint_text, Vector2 position, Color text_color) {
        this.gameObject.name = "Pop up text (" + hint_text + ")";
        rect_transform = this.gameObject.AddComponent<RectTransform>();
        rect_transform.SetParent(GameObject.Find("Overlay UI").transform);
        rect_transform.localScale = Vector3.one;
        rect_transform.position = new Vector3(position.x, position.y, 0);
        Text text = this.gameObject.AddComponent<Text>();
        text.text = hint_text;
        text.color = text_color;
        text.alignment = TextAnchor.MiddleCenter;
        text.font = GameObject.Find("Overlay UI").GetComponent<Text>().font;
    }

	void Start () {
        Destroy(this.gameObject, 3f);
	}
	
	void Update () {
        rect_transform.Translate(Vector2.up / 6f * Time.deltaTime);
	}
}
