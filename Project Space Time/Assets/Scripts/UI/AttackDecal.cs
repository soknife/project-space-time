using UnityEngine;
using System.Collections;

public class AttackDecal : MonoBehaviour {

    Vector2 position;

    public void Create(Vector2 location, Sprite decal) {
        gameObject.name = "Decal";
        gameObject.transform.position = new Vector3(location.x, location.y, 0);
        gameObject.AddComponent<SpriteRenderer>().sprite = decal;
        gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Decal";
    }

	void Start () {
	    Destroy(this.gameObject, 0.1f);
	}
	
}
