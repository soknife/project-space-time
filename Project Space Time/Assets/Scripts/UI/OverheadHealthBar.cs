using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OverheadHealthBar : MonoBehaviour {

    RectTransform rect_transform;
    Character target_character;
    Image health_bar;

    public void Create(Character target) {
        this.gameObject.name = target.name + " health bar";
        rect_transform = this.gameObject.AddComponent<RectTransform>();
        rect_transform.SetParent(GameObject.Find("Overlay UI").transform);
        rect_transform.localScale = Vector3.one;
        rect_transform.position = new Vector3(target.gameObject.transform.position.x,
            target.gameObject.transform.position.y + 0.5f, 0);
        rect_transform.localScale = new Vector3((float)target.state.health / target.state.max_health * 1, 0.1f, 1);
        health_bar = this.gameObject.AddComponent<Image>();
        target_character = target;
        Canvas canvas = this.gameObject.AddComponent<Canvas>();
        canvas.overrideSorting = true;
        canvas.sortingLayerName = "Player";
        canvas.sortingOrder = 1;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        rect_transform.position = new Vector3(target_character.gameObject.transform.position.x,
            target_character.gameObject.transform.position.y + 0.5f, 0);
        rect_transform.localScale = new Vector3(
            (float)target_character.state.health / target_character.state.max_health * 1, 0.1f, 1);
	}
}
