using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Stores items of type Item
// This class is used to store all items in game
// A derived version of this class Inventory stores items on the player
public class ItemDatabase : MonoBehaviour {

    [Tooltip("Adds these item into database upon instantiate")]
    public Item[] quick_load;

    [SerializeField]
    protected List<Item> items = new List<Item>();

    void Awake() {
        this.Initialize();
    }

    // Loads items from quick_load into items
    virtual protected void Initialize() {
        foreach (Item item in quick_load) {
            this.AddItem(item);
        }
    }

    // Add item to items, returns 0 if successful, otherwise returns error number
    virtual public bool AddItem(Item item) {
        // Item with id already exists
        if (FindItem(item.id) != null)
            return false;
        items.Add(item);
        return true;
    }

    // Find first instance of item with name in items, returns item
    public Item FindItem(string name) {
        return items.Find(x => x.name == name);
    }


    // Find all items with id in items, returns list
    public List<Item> FindItem(uint id) {
        if (items.FindAll(x => x.id == id).Count == 0)
            return null;
        return items.FindAll(x => x.id == id);
    }

    // Removes item with name in items, returns true if successful
    virtual public bool RemoveItem(string name) {
        int index = items.FindIndex(x => x.name == name);
        if (index == -1)
            return false;
        items.RemoveAt(index);
        return true;
    }

    // Removes all items with id in items, returns true if successful
    virtual public bool RemoveItem(uint id) {
        int count = items.RemoveAll(x => x.id == id);
        if (count == 0)
            return false;
        return true;
    }

    

    // Returns item array of items
    public Item[] GetItems() {
        return items.ToArray();
    }
}
