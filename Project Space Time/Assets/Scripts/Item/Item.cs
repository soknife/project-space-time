using UnityEngine;
using System.Collections;

// The item class should be added as an component to an empty gameobject.
// That gameobjects' only purpose should be to provide a prefab, and provide instance variables.
// Do not add a gameobject with component of type item into a scene.
// Do not call start(), update() or similar mono functions from this
public class Item : MonoBehaviour {

    [Tooltip("Item name \nMay not be unique \nMay not necessarily be the parent gameobject's name")]
    new public string name;
    public string use_hint;
    [Tooltip("Item id as integer \nUnique for each item, but same for all instances of the item")]
    public uint id;
    [Tooltip("Can this item be picked up")]
    public bool lootable;
    [Tooltip("Is this item unique in inventory (Cannot have duplicate in inventory)")]
    public bool unique;
    [Tooltip("How many of these items are in this instance")]
    public int count;
    [Tooltip("Is this item stackable")]
    public bool stackable;
    [Tooltip("Item display")]
    public Sprite image;

}
