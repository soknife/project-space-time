![Logo](https://bitbucket.org/soknife/project-space-time/raw/master/Project%20Space%20Time/Assets/Sprites/hypercube_logo.png)

#Project space time#
Yusheng s3429221, Adrian s3292955

##Overview##
Project space time is a 2D top down rogue like game, with an important mechanic of world persistence between deaths. Similar to other rogue likes, the world is procedurally generated. However, on death, the player is given a choice to respawn in the same world after a period of time has passed. Depending on how long time has passed, the world will evolve and change.

##Game components to Implement##
Map generation 

Player control

Item usage

NPC

Time progression

##Assessment criteria##
PA: Basic grid system, player movement and actions, items, and enemies.

CR: Map details with various tile types, inventory and equipment management, more 
interactions with items and enemies.

DI: Basic map generation, basic time progression, polished UI (from title menu to death 
screen)

HD: Path finding (enables better map generation, enemy AI etc.).